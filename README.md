# Rules for the [Linux Gaming](https://discord.gg/linuxgaming) Discord

## Server Code of Conduct

### 1. Be respectful to the server as a community and all of its users.

Be nice. Treat others the way you would want to be treated. This is a respectful and inclusive community.

#### 1a. No Discrimination

- Discrimination based on gender, race, religion, disability, or sexuality is strictly forbidden.
- This includes any and all hate speech, use of contemptuous terms, harassment, and reinforcing social prejudices against minority groups.

#### 1b. No trolling

> A "troll" is a person who misuses their chat privileges to intentionally disrupt, cause controversy, incite an argument, and/or receive negative attention by deliberately posting provocative content.
> The term may also be used as a verb, to refer to the act of posting such content, or as a noun, to refer the content itself.

- Trolls can be deceitful and frequently use indirect expressions of hostility through ambivalence and implicit messages as a method of covertly insulting, intimidating, or inciting a person or persons for their own sadistic pleasure.
- They often pick their words very carefully and are therefore able to defend their masked attempts at creating unrest, redirecting the blame onto the community and its supposed failure to understand them properly.

> **Trolling is prohibited.**

#### 1c. No flaming

> Flaming, in the most common sense definition, is directing negative, disrespectful, and/or insulting comments toward someone.

- An equally or more negative response, resulting in a cycling exchange of insults is often the consequence.
- Flaming fellow members (including staff) will not be tolerated.
- Avoid personal insults and sarcastic or patronizing language.
- _"Discussions can be productive, but quarreling is always destructive."_

#### 1d. No harassment, threats of violence, or threats of self harm

- Do not call for violence or threaten other people, whether they're fellow members or not. We take threats of violence very seriously and will act accordingly.
- Do not threaten to kill yourself, this will result in a permanent ban. Moderators are not qualified to deal with these kinds of instances.
- Do not suggest that someone should harm themselves, this will result in a permanent ban.
- If you are seriously in a bad mental state and are actively considering self harm, please call one of the numbers listed here: https://en.wikipedia.org/wiki/List_of_suicide_crisis_lines
- False pings to Admins and Moderators are not allowed. If you ping one of these roles, please include context of what you need help with.
- Sending/Linking any harmful material such as viruses, IP grabbers or harmware results in an immediate and permanent ban.

### 2. Content Guidelines

In the interest of safety and fostering a healthy community, we have a few rules we'd like members to follow while chatting.

#### 2a. English

- Conversations (textual and vocal) should be kept to English.

#### 2b. NSFW content

- Do not have sexual or graphic conversations in voice or text channels.
- Posting **ANY** NSFW/NSFL content is strictly prohibited. This includes images and discussions that make allusions to sexual behavior or fetishes.

#### 2c. Piracy

- While discussions about piracy are allowed, linking to pirated resources, encouraging users to pirate, or naming websites that enable piracy is not allowed.

#### 2d. Advertising and Self Promotion

- Self promotion is generally allowed, but not in excessive quantities. The moderators will warn you if it gets excessive and may escalate the issue if warnings are ignored.

#### 2e. Drugs and Controlled Substances

- Discussions about drugs and other controlled substances is generally allowed, but linking to resources that sell, help you acquire, or teach you to make these controlled substances is not allowed.

#### 2f. Spam

Don't post excessively; try not to monopolize the chat.

Our resident moderator bot:

- has an anti-spam function and will take action if it detects spam.
- will tolerate 10 messages it considers spam before muting a user.
- will remove duplicate messages.
- will mute people for 5 minutes if they send too many messages at a time.

#### 2g. Off-topic

Off-topic discussion is fine within reason. However, there is very low tolerance for off-topic discussion in the following scenarios:

- Shitposting outside of #offtopic
- Off-topic chat in any sort of tech support channel
- Bot commands outside of #bot-commands (unless relevant to the conversation)

#### 2h. AutoMod

- To keep chat clear of slurs and other problematic phrases, we've employed the use of Discord's AutoMod feature.

#### 2i. Don't get creative with rule breaking

- Staying borderline to the rules or finding loopholes will be treated as breaking the rules.

## Moderation Guidelines

### Basic Principles

- When people first join the server, **the initial culture and conversations they see act as a powerful example.**

  - If the first people see when they join is high-quality discussion, they will expect it to be the standard and will try to uphold that standard.
  - Mods should strongly influence conversations toward meaningful, high-quality interactions. Shitposting should be steered away from discussion channels.

- Instead of reactive moderation and focusing on punishments and infraction, **treat people as humans** and ask them to change the way they're acting.

  - We do not implement a strike system, a point system, or any kind of progressive punishment system.

    - Timed mutes and short-term bans are ineffective at resolving conflicts, and only seem to work because they briefly remove the source.
    - Systems like these only multiply the amount of needed moderation work, push people away, and create a culture of fear around moderators.

  - Staff should not be combative or aggressive towards members, especially not in retaliation.
  - Most conflict is caused by a small set of **recalcitrant, high-conflict people.** Identifying these people early, before they can have a large effect on the server culture, is crucial.

- Feel free to **ask any questions** you have, even if they seem obvious, or bring up any problems you may have with rules, decisions, or other moderators.
  - If you do not feel comfortable raising these concerns in public, always feel free to DM an Admin.
- If you're not sure on something, consult with the rest of the mod team in #mod-chat, but don't ignore it.
- **Don't be afraid to act.** If someone, even a long-time member of the community or a member of staff, is causing issues, don't hesitate to pull them out of the conversation and ask them about it. People who are resistant to changing the way they interact with the community might not be the best fit to stay in it.

### Rules

- **You need the explicit consensus of at least one other moderator before issuing a ban.**
  - This does not apply in cases of clear spam, blatantly NSFW content, overt bigotry, racial slurs, raids, pre-emptive bot bans, etc.
- Use mutes to pull out users from heated discussion or to talk to them privately. **Do not use mutes as a punishment.**
  - Use meaningful mute descriptions, not just "talk" or "private chat". This helps give context to future actions.
  - If you forget to do so, leave a note using `!addWarning`.
- If you give anyone a verbal warning, use the `!addWarning` command to **log it as a note** for the other moderators.
  - The user will not be notified of this.
- **Never perform moderation actions manually.** Always use the moderation commands. This makes sure all actions are logged.
- Make sure to let people know if they use channels incorrectly, including asking for support outside the support category.
- If you notice that a member is contributing positively and consistently to the server culture or to the support channels, **feel free to give them the helper role** after getting the sign-off of another moderator.

### Handling Disruptive Users

- If we notice someone causing a disruption in the server, we bring this up in chat and talk to them. If they seem receptive, that's where it ends and we log the fact that the conversation took place in the moderation logs.
- If there is a heated conversation happening, we evaluate if it needs moderator intervention. If so, we **find the person at the core of the argument**, mute them, and talk to them. Follow the same procedure as before.
- If there's a continued pattern of disruption, we mute them, and ask them about it.
  - Do they recognize what they are doing?
  - Do they seem to be receptive and willing to change their interactions?
  - Assume good faith and act accordingly. Talking to users helps us understand their original intent and lets us determine if the root of the issue was a misunderstanding.
  - It is important to emphasize that this is a conversation, not a punishment.
  - If they are receptive, we log the conversation and give them another chance.
  - If they say they agree but do not actually change anything, or are combative and ignore the conversation, we explain the situation and issue a **long-term ban** (at least 30 days).

## Support Rules

- State what you need help with off the bat. [Don't ask to ask.](https://dontasktoask.com/)
- Give context to your issue preemptively. [Follow the standard litany.](https://www.co.kerr.tx.us/it/howtoreport.html)
- You are not owed support. [Don't be a help vampire.](https://slash7.com/2006/12/22/vampires/)
- Do not DM people asking for support without prior permission.
- Do not post your question in multiple channels.
- Do not post your question in non-support channels.
- Do not use the enter key as a full-stop. Please speak in full sentences.
- Do not @ the Helpers with questions that are not emergencies.
- Do not @ any volunteer that is not currently active in a support channel.
- Do not ask for support for pirated/illegal software.
- Prefer text pastes over screenshots or cameras. Where you don't have an environment to use pastes use this and provide the link: `<command to print output> |& curl --data-binary @- https://paste.rs`

# References and Attributions

**This document was created by referencing the following documents:**

- [The Arch Linux Code of Conduct](https://terms.archlinux.org/docs/code-of-conduct/)
- [The Fedora Community Code of Conduct](https://docs.fedoraproject.org/en-US/project/code-of-conduct/)
- [The Archlinux Discord's Server Code of Conduct by Anna Kudriavtsev](https://hackmd.io/@annaaa/arch#Moderation-Guidelines)
